import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

function loadLocaleMessages () {
  const locales = require.context('./locales', true, /[A-Za-z0-9-_,\s]+\.json$/i)
  const messages = {}
  locales.keys().forEach(key => {
    const matched = key.match(/([A-Za-z0-9-_]+)\./i)
    if (matched && matched.length > 1) {
      const locale = matched[1]
      messages[locale] = locales(key)
    }
  })
  return messages
}

const dateTimeFormats = {
  'en-US': {
    dateShort:{
      year: 'numeric',
      month: 'short',
      day: 'numeric'
    },
    dateLong:{
      year: 'numeric',
      month: 'long',
      day: 'numeric'
    },
    date2Digit:{
      year: 'numeric',
      month: '2-digit',
      day: 'numeric'
    },
    dateNumeric:{
      year: 'numeric',
      month: 'numeric',
      day: 'numeric'
    },
    timeShort:{
      hour: 'numeric',
      minute: 'numeric'
    },
    timeMedium:{
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
    },
    timeLong:{
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
      timeZoneName: 'short'
    },
    short:{
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric'
    },
    medium: {
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric'
    },
    long:{
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      timeZoneName: 'short',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric'
    }
  },
  'ar-EG': {
    dateShort:{
      year: 'numeric',
      month: 'short',
      day: 'numeric'
    },
    dateLong:{
      year: 'numeric',
      month: 'long',
      day: 'numeric'
    },
    date2Digit:{
      year: 'numeric',
      month: '2-digit',
      day: 'numeric'
    },
    dateNumeric:{
      year: 'numeric',
      month: 'numeric',
      day: 'numeric'
    },
    timeShort:{
      hour: 'numeric',
      minute: 'numeric'
    },
    timeMedium:{
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
    },
    timeLong:{
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
      timeZoneName: 'short'
    },
    short:{
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric'
    },
    medium: {
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric'
    },
    long:{
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      timeZoneName: 'short',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric'
    }
  },
  'ko-KR':{
    dateShort:{
      year: 'numeric',
      month: 'short',
      day: 'numeric'
    },
    dateLong:{
      year: 'numeric',
      month: 'long',
      day: 'numeric'
    },
    date2Digit:{
      year: 'numeric',
      month: '2-digit',
      day: 'numeric'
    },
    dateNumeric:{
      year: 'numeric',
      month: 'numeric',
      day: 'numeric'
    },
    timeShort:{
      hour: 'numeric',
      minute: 'numeric'
    },
    timeMedium:{
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
    },
    timeLong:{
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
      timeZoneName: 'short'
    },
    short:{
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric'
    },
    medium: {
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric'
    },
    long:{
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      timeZoneName: 'short',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric'
    }

  },
  'ru-RU':{
    dateShort:{
      year: 'numeric',
      month: 'short',
      day: 'numeric'
    },
    dateLong:{
      year: 'numeric',
      month: 'long',
      day: 'numeric'
    },
    date2Digit:{
      year: 'numeric',
      month: '2-digit',
      day: 'numeric'
    },
    dateNumeric:{
      year: 'numeric',
      month: 'numeric',
      day: 'numeric'
    },
    timeShort:{
      hour: 'numeric',
      minute: 'numeric'
    },
    timeMedium:{
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
    },
    timeLong:{
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
      timeZoneName: 'short'
    },
    short:{
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric'
    },
    medium: {
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric'
    },
    long:{
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      timeZoneName: 'short',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric'
    }
  }
}

export default new VueI18n({
  locale: process.env.VUE_APP_I18N_LOCALE || 'en',
  fallbackLocale: process.env.VUE_APP_I18N_FALLBACK_LOCALE || 'en',
  messages: loadLocaleMessages(),
  dateTimeFormats
})
