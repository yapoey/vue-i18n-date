import Vue from 'vue'
import VueRouter from 'vue-router'
import LocaleString from "../views/LocaleString";
import I18n from "../views/I18n"
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'localeString',
    component: LocaleString
  },
  {
    path: '/i18n',
    name: 'i18n',
    component: I18n
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
